var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('scripts', function() {
    gulp.src([
            'node_modules/jquery/dist/jquery.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'src/js/*.js'
        ])
        .pipe(concat('script.js'))
        .pipe(gulp.dest('dist/js'))
});

gulp.task('styles', function() {
    gulp.src([
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'src/css/*.css'
        ])
        .pipe(concat('style.css'))
        .pipe(gulp.dest('dist/css'))

    // Handle the bootstrap css min file to avoid console warnings
    gulp.src("node_modules/bootstrap/dist/css/bootstrap.min.css.map")
        .pipe(gulp.dest('dist/css'))

    // Handle ie7
    gulp.src("src/css-ie7/bootstrap-ie7.css")
        .pipe(gulp.dest('dist/css'))
});

gulp.task('images', function() {
    gulp.src([
            'src/images/**/*'
        ])
        .pipe(gulp.dest('dist/images'))
});

gulp.task('fonts', function() {
    gulp.src([
            'src/fonts/**/*'
        ])
        .pipe(gulp.dest('dist/fonts'))
});

gulp.task('default', function() {
    gulp.run('scripts', 'images', 'styles', 'fonts');
});

gulp.task('watch', function() {
    gulp.run('default');

    gulp.watch('src/js/*.js', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        gulp.run('scripts');
    });

    gulp.watch('src/images/**/*', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        gulp.run('images');
    });

    gulp.watch('src/fonts/**/*', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        gulp.run('fonts');
    });

    gulp.watch('src/css/*.css', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        gulp.run('styles');
    });
});